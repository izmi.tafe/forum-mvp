let API_ROUTE;

process.env.NODE_ENV === "development"
    ? (API_ROUTE = "http://35.170.150.133:80/api/v1")
    : (API_ROUTE = "http://34.229.15.200:80/api/v1");

export default API_ROUTE;
