import React from 'react';
import "./Sidebar.css";

export default function Sidebar() {
    return (
        // <div className="list-kiri card"> 
            // <a href="/dashboard"><button className="btn btn-primary btn-block">Home</button></a>
            // <a href="/trending"><button className="btn btn-primary btn-block">Trending</button></a>
            // <a href="/group"><button className="btn btn-primary btn-block">Group</button></a>
            // <a href="/project"><button className="btn btn-primary btn-block">Project</button></a>
        //     </div>
        <div className="sidebar">
            <a href="/">Dashboard</a>
            <a href="/trending">Trending</a>
            <a href="/preference">Preference</a>
            <a href="/group">Group</a>
        </div>
    )
}
