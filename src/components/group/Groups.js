import React from 'react'
import { Link } from 'react-router-dom'

import Sidebar from './../Sidebar.js';
import Group from './Group';


const Groups = () => {


  return (
    <>
      <Sidebar />
    <div className="content">
      <div className="container card">
        <h3 className="text-center mt-3"><b>Group List</b></h3>
            <hr />
            <div className="container">
                <div  className="mt-2 style-card" >
                    <Link to={'/groups/2' } >
                         <Group/>
                    </Link>
                </div>
            </div>
        </div>
    </div>
    </>
  )
}

export default Groups
