import React from "react";
import { Card, CardText, CardBody, CardTitle } from "reactstrap";
import "./Groups.css";

const Group = () => {
    return (<>
       
       
        <Card className="style-card-main">
            <CardBody className="style-card-body">
                
                <CardTitle className=""><h4>GROUP #1</h4></CardTitle>
                <CardTitle className=""><h6>Jumlah Anggota : 203</h6></CardTitle>
                <CardTitle className=""><h6>DESKRIPSI GROUP :</h6></CardTitle>
                <CardText>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</CardText>
                {/* <CardText>{post.tag}</CardText> */}
                
            </CardBody>

            
        </Card>

        <Card className="style-card-main">
            <CardBody className="style-card-body">
                
                <CardTitle className=""><h4>GROUP #2</h4></CardTitle>
                <CardTitle className=""><h6>Jumlah Anggota : 103</h6></CardTitle>
                <CardTitle className=""><h6>DESKRIPSI GROUP :</h6></CardTitle>
                <CardText>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</CardText>
                {/* <CardText>{post.tag}</CardText> */}
                
            </CardBody>

            
        </Card>
        
        </>

        
    );
};

export default Group;
